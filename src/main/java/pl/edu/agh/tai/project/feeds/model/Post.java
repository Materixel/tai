package pl.edu.agh.tai.project.feeds.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Janusz on 2015-05-27.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @ManyToOne
//    @NotNull
    @Cascade({CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.SAVE_UPDATE})
    @JoinColumn
    private User creator;

    @ManyToOne
    @NotNull
    @Cascade({CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.SAVE_UPDATE})
    @JoinColumn
    private Feed feed;

    @NotNull
    private String content;

    @OneToMany(mappedBy = "post")
    @Cascade(CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    @NotNull
    private Date creationDate;

    private Date lastEditionDate;

    public Post() {

    }

    public Post(User creator,String content){
        this.creator = creator;
        this.content = content;
        this.creationDate = new Date();
        this.comments = new LinkedList<>();
    }

    public void addComment(Comment newComment){
        this.comments.add(newComment);
    }

    //Begin getters and setters.
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator= creator;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastEditionDate() {
        return lastEditionDate;
    }

    public void setLastEditionDate(Date lastEditionDate) {
        this.lastEditionDate = lastEditionDate;
    }

    //End getters and setters.
}
