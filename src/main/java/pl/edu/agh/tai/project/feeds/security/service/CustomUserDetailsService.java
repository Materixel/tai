package pl.edu.agh.tai.project.feeds.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.List;

/**
 * Created by Michal on 2015-05-27.
 */
@Service(value = "userDetailsService")
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s);
    }
}
