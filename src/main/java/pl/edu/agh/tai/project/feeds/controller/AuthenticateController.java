package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CreateAccountForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.exception.FieldErrorException;
import pl.edu.agh.tai.project.feeds.exception.UserAlreadyExistsException;
import pl.edu.agh.tai.project.feeds.model.Role;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.validation.Valid;
import java.util.Arrays;

/**
 * Created by Michal on 2015-06-07.
 */
@Controller
@RequestMapping("auth")
public class AuthenticateController {
    @Autowired
    UserService userService;

    @Autowired
    @Qualifier("createAccountFormValidator")
    private Validator validator;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private ProviderSignInUtils providerSignInUtils;

    @InitBinder("createAccountForm")
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(validator);
    }

    @ModelAttribute("createAccountForm")
    public CreateAccountForm getCreateAccountForm() {
        return new CreateAccountForm();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String getLoginPage(@RequestParam(value = "error", required = false, defaultValue = "false") Boolean error, Model model) {
        if (error) {
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", "Niepoprawny login lub haslo");
        }
        return "auth/login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/create")
    public String showCreateAccountForm(WebRequest request, Model model) {
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        if (connection != null) {
            User user = createAccountForFacebookUser(connection);
            try {
                userService.save(user);
            } catch (UserAlreadyExistsException e) {
                e.printStackTrace();
            }
            providerSignInUtils.doPostSignUp(user.getUserId(), request);
            return "redirect:/auth/facebook";
        }
        model.addAttribute("roles", Arrays.asList(Role.COMMENTER, Role.CREATOR));
        return "auth/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createAccount(@Valid @ModelAttribute("createAccountForm") CreateAccountForm createAccountForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes, WebRequest request) {
        if (bindingResult.hasErrors()) {
            return handleCreateRedirect(createAccountForm, bindingResult, redirectAttributes);
        }
        User user = createAccountForNormalUser(createAccountForm);
        try {
            userService.save(user);
        } catch (UserAlreadyExistsException e) {
            return handleCreateRedirect(createAccountForm, bindingResult, redirectAttributes);
        }
        try {
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, createAccountForm.getPassword(), user.getAuthorities());
            authenticationManager.authenticate(auth);

            // redirect into secured main page if authentication successful
            if(auth.isAuthenticated()) {
                SecurityContextHolder.getContext().setAuthentication(auth);
                return "redirect:/";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }

    private String handleCreateRedirect(CreateAccountForm createAccountForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", true);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.createAccountForm",
                bindingResult);
        redirectAttributes.addFlashAttribute("createAccountForm", createAccountForm);
        return "redirect:create";
    }

    @ExceptionHandler(FieldErrorException.class)
    private String fieldErrorExceptionHandel(FieldErrorException exception, @ModelAttribute("createAccountForm") CreateAccountForm createAccountForm,
                                             BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        bindingResult.addError(new FieldError("createAccountForm", exception.getField(), exception.getErrorKey()));
        return handleCreateRedirect(createAccountForm, bindingResult, redirectAttributes);
    }

    public User createAccountForFacebookUser(Connection<?> connection) {
        UserProfile profile = connection.fetchUserProfile();
        User user = new User();
        user.setEmail(profile.getEmail());
        user.setSignInProvider("facebook");
        user.setUsername(connection.getKey().getProviderUserId());
        user.setName(profile.getName());
        user.setPassword("facebook");
        user.setRoles(Arrays.asList(Role.CREATOR, Role.COMMENTER, Role.FACEBOOK));
        return user;
    }

    public User createAccountForNormalUser(CreateAccountForm createAccountForm) {
        User user = new User();
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        user.setUsername(createAccountForm.getUsername());
        user.setName(createAccountForm.getUsername());
        user.setPassword(encoder.encodePassword(createAccountForm.getPassword(), null));
        user.setEmail(createAccountForm.getEmail());
        user.setRoles(createAccountForm.getRoles());
        return user;
    }

}
