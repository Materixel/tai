package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.edu.agh.tai.project.feeds.model.Role;

import java.security.Principal;

/**
 * Created by Michal on 2015-06-05.
 */
@Controller
@RequestMapping("/")
public class StartController {

    @RequestMapping(method = RequestMethod.GET, value = "403")
    public String showErrorPage() {
        return "start/403";
    }

    @RequestMapping
    public String index() {
        return "redirect:/feeds";
    }
}
