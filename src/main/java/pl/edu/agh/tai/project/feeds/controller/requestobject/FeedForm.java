package pl.edu.agh.tai.project.feeds.controller.requestobject;

import java.util.Date;

/**
 * Created by Janusz on 2015-06-05.
 */
public class FeedForm {
    private String content;

    private String title;

    private String location;

    private Date startTime;

    private Date endTime;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
