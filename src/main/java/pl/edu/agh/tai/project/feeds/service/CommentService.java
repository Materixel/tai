package pl.edu.agh.tai.project.feeds.service;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Comment;

import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */

public interface CommentService {

    public List<Comment> list();

    List<Comment> findByPost_Id(Integer id);

    public void save(Comment feed);

    Comment get(Integer id) throws EntityNotExistsException;

    @Transactional
    @PreAuthorize("#comment.creator.username == principal.username")
    void delete(Comment comment) throws EntityNotExistsException;
}
