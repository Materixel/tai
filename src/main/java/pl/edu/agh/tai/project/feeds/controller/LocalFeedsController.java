package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FeedForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.*;
import pl.edu.agh.tai.project.feeds.service.LocalFeedService;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

/**
 * Created by Michal on 2015-07-01.
 */
@Controller
@RequestMapping("feeds/local")
public class LocalFeedsController {
    @Autowired
    private LocalFeedService localFeedService;

    @Autowired
    private UserService userService;


    @Autowired
    @Qualifier("localFeedFormValidator")
    private Validator validator;

    @InitBinder("feedForm")
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(validator);
    }

    @ModelAttribute("feedForm")
    public FeedForm addFeedFormToModel(@PathVariable Map<String, String> params) {
        FeedForm form = new FeedForm();
        if (params.containsKey("feed_id")) {
            final LocalFeed feed;
            try {
                feed = localFeedService.get(Integer.valueOf(params.get("feed_id")));
            } catch (EntityNotExistsException | NumberFormatException e) {
                return form;
            }
            setPropertiesToFormFromLocalFeed(feed, form);
        }
        return form;
    }

    @Secured("ROLE_CREATOR")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreateFeedForm() {
        return "feeds/local/create";
    }

    @Secured({"ROLE_CREATOR"})
    @RequestMapping(value = "/{feed_id}/edit", method = RequestMethod.GET)
    public String showEditFeedForm() {
        return "feeds/local/edit";
    }

    @Secured("ROLE_CREATOR")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createFeed(@Valid @ModelAttribute("feedForm") FeedForm feedForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes,
                             Principal principal){

        if (bindingResult.hasErrors()) {
            return handleFormRedirect(feedForm, "create", bindingResult, redirectAttributes);
    }

        LocalFeed newFeed = new LocalFeed();

        setPropertiesToLocalFeedFromForm(newFeed, feedForm);

        String username = principal.getName(); //get logged in username
        User creator = null;
        try {
            creator = userService.findByUsername(username);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }
        newFeed.setCreator(creator);

        localFeedService.save(newFeed);
        return "redirect:/feeds/" + newFeed.getId();
    }

    @Secured({"ROLE_CREATOR"})
    @RequestMapping(value = "/{feed_id}/edit", method = RequestMethod.POST)
    public String editFeed(@Valid @ModelAttribute("feedForm") FeedForm feedForm,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes,
                           Principal principal, @PathVariable("feed_id") Integer feedId){

        if (bindingResult.hasErrors()) {
            return handleFormRedirect(feedForm, "edit", bindingResult, redirectAttributes);
        }

        LocalFeed editedFeed = null;
        try {
            editedFeed = localFeedService.get(feedId);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }

        //assume that we don't change creator, as finally only creator should be able to edit.
        setPropertiesToLocalFeedFromForm(editedFeed, feedForm);

        localFeedService.save(editedFeed);
        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{feed_id}", method = RequestMethod.GET)
    public String showFeed(@PathVariable("feed_id") Integer feedId, Model model,
                           RedirectAttributes redirectAttributes) {
        Feed feed;
        try {
            feed = localFeedService.get(feedId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Wydarzenie nie istnieje");
            return "redirect:/feeds";
        }
        feed.getPosts().sort(new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                Date d1;
                Date d2;
                if (o1.getLastEditionDate() != null) {
                    d1 = o1.getLastEditionDate();
                } else {
                    d1 = o1.getCreationDate();
                }
                if (o2.getLastEditionDate() != null) {
                    d2 = o2.getLastEditionDate();
                } else {
                    d2 = o2.getCreationDate();
                }
                return d1.compareTo(d2);
            }
        });
        for (Post p: feed.getPosts()) {
            p.getComments().sort(new Comparator<Comment>() {
                @Override
                public int compare(Comment o1, Comment o2) {
                    Date d1;
                    Date d2;
                    if (o1.getLastEditionDate() != null) {
                        d1 = o1.getLastEditionDate();
                    } else {
                        d1 = o1.getCreationDate();
                    }
                    if (o2.getLastEditionDate() != null) {
                        d2 = o2.getLastEditionDate();
                    } else {
                        d2 = o2.getCreationDate();
                    }
                    return d1.compareTo(d2);
                }
            });
        }
        model.addAttribute("feed", feed);
        return "feeds/local/show";
    }

    @Secured({"ROLE_CREATOR"})
    @RequestMapping(value = "/{feed_id}/delete", method = RequestMethod.GET)
    public String deleteFeed(@PathVariable("feed_id") Integer feedId, RedirectAttributes redirectAttributes) {
        try {
            localFeedService.delete(localFeedService.get(feedId));

        } catch (DataIntegrityViolationException e) {
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/feeds";
        } catch (EntityNotExistsException e1) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Feed o podanym id nie istnieje");
            return "redirect:/feeds";
        }
        redirectAttributes.addFlashAttribute("successMessage", "Feed zosta� usuni�ty");
        return "redirect:/feeds";
    }

    private String handleFormRedirect(FeedForm feedForm, String location, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", true);
        redirectAttributes.addFlashAttribute("feedForm", feedForm);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.feedForm",
                bindingResult);
        return "redirect:" + location;
    }

    private void setPropertiesToLocalFeedFromForm(LocalFeed localfeed, FeedForm feedForm){
        localfeed.setContent(feedForm.getContent());
        localfeed.setTitle(feedForm.getTitle());
        localfeed.setLocation(feedForm.getLocation());
        localfeed.setStartTime(feedForm.getStartTime());
        localfeed.setEndTime(feedForm.getEndTime());
    }

    private void setPropertiesToFormFromLocalFeed(LocalFeed localfeed, FeedForm feedForm){
        feedForm.setContent(localfeed.getContent());
        feedForm.setTitle(localfeed.getTitle());
        feedForm.setLocation(localfeed.getLocation());
        feedForm.setStartTime(localfeed.getStartTime());
        feedForm.setEndTime(localfeed.getEndTime());
    }
}
