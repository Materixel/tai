package pl.edu.agh.tai.project.feeds.controller.requestobject;

/**
 * Created by Michal on 2015-07-01.
 */
public class FacebookFeedForm {
    private String eventId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
