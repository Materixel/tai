package pl.edu.agh.tai.project.feeds.service.impl;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.facebook.api.Event;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.FacebookFeedRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.FacebookFeed;
import pl.edu.agh.tai.project.feeds.service.FacebookFeedService;
import pl.edu.agh.tai.project.feeds.service.FacebookService;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michal on 2015-07-01.
 */
@Service
public class FacebookFeedServiceImpl implements FacebookFeedService {
    @Autowired
    private FacebookFeedRepository facebookFeedRepository;

    @Autowired
    private FacebookService facebookService;

    @Override
    @Transactional
    public void save(FacebookFeed facebookFeed) {
        facebookFeedRepository.save(facebookFeed);
    }

    @Override
    @Transactional
    public List<Pair<Integer, Event>> listAsPairs() {
        List<FacebookFeed> facebookEventsOurs = list();
        List<Pair<Integer, Event>> facebookEvents = new LinkedList<>();
        for(FacebookFeed ourEvent: facebookEventsOurs){
            facebookEvents.add(new Pair<>(ourEvent.getId(), facebookService.getEvent(ourEvent.getEventId())));
        }
        return facebookEvents;
    }


    @Override
    @Transactional
    public List<FacebookFeed> list() {
        return facebookFeedRepository.findAll();
    }

    @Override
    @Transactional
    public FacebookFeed getByEventId(String eventId) {
        return facebookFeedRepository.getByEventId(eventId);
    }

    @Override
    @Transactional
    public FacebookFeed get(Integer feedId) throws EntityNotExistsException {
        FacebookFeed feed = facebookFeedRepository.findOne(feedId);
        if (feed == null) {
            throw new EntityNotExistsException("Post with id " + feedId + " doesn't exist");
        }
        return feed;
    }

    @Override
    @Transactional
    @PreAuthorize("#facebookFeed.creator.username == principal.username")
    public void delete(FacebookFeed facebookFeed) {
        facebookFeedRepository.delete(facebookFeed);
    }


}
