package pl.edu.agh.tai.project.feeds.service;

import org.springframework.security.access.prepost.PreAuthorize;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.User;

import java.security.Principal;
import java.util.List;

/**
 * Created by Janusz on 2015-06-01.
 */
public interface LocalFeedService {

    List<LocalFeed> findByCreator(User creator);

    List<LocalFeed> list();

    void save(LocalFeed feed);

    LocalFeed get(Integer id) throws EntityNotExistsException;

    void delete(LocalFeed localFeed);

//    void delete(Integer id) throws EntityNotExistsException;
}
