package pl.edu.agh.tai.project.feeds.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FeedForm;
import pl.edu.agh.tai.project.feeds.controller.utils.ValidationUtils;

/**
 * Created by Janusz on 2015-06-18.
 */
@Component
public class LocalFeedFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return FeedForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final FeedForm feedForm = (FeedForm) object;
        validateTitle(feedForm, errors);
        validateContent(feedForm,errors);
        validateDates(feedForm,errors);
    }

    private void validateTitle(FeedForm feedForm, Errors errors) {
        if (!ValidationUtils.isStringNotNullAndNonEmpty(feedForm.getTitle())) {
            errors.rejectValue("title", "form.feed.title.empty");
            return;
        }
    }

    private void validateContent(FeedForm feedForm, Errors errors) {
        if (!ValidationUtils.isStringNotNullAndNonEmpty(feedForm.getContent())) {
            errors.rejectValue("content", "form.feed.content.empty");
            return;
        }
    }

    private void validateDates(FeedForm feedForm, Errors errors){
        if(feedForm.getStartTime()==null){
            if(feedForm.getEndTime()!=null){
                errors.rejectValue("endTime","form.feed.endTime.noStartTime");
                return;
            }
        }
        if (!ValidationUtils.isEndDateNotEarlierThanStart(feedForm.getStartTime(), feedForm.getEndTime())){
            errors.rejectValue("endTime","form.feed.endTime.earlierThanStartTime");
        }
    }
}