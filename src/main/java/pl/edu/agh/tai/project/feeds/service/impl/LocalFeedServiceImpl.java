package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.LocalFeedRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.LocalFeedService;

import java.security.Principal;
import java.util.List;

/**
 * Created by Janusz on 2015-06-01.
 */
@Service
public class LocalFeedServiceImpl implements LocalFeedService {

    @Autowired
    private LocalFeedRepository localFeedRepository;

    @Override
    @Transactional
    public List<LocalFeed> findByCreator(User creator) {
        return localFeedRepository.findByCreator(creator);
    }

    @Override
    @Transactional
    public List<LocalFeed> list() {
        return localFeedRepository.findAll();
    }

    @Override
    @Transactional
    @PreAuthorize("#feed.creator.username == principal.username")
    public void save(LocalFeed feed) {
        localFeedRepository.save(feed);
    }

    @Override
    @Transactional
    public LocalFeed get(Integer id) throws EntityNotExistsException {
        LocalFeed feed = localFeedRepository.findOne(id);
        if (feed == null) {
            throw new EntityNotExistsException("Feed with id " + id + " doesn't exist");
        }
        return feed;
    }

    @Override
    @PreAuthorize("#feed.creator.username == principal.username")
    @Transactional
    public void delete(LocalFeed feed) {
        localFeedRepository.delete(feed);
    }
}
