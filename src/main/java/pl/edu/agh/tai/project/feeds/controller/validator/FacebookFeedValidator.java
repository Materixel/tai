package pl.edu.agh.tai.project.feeds.controller.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Event;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FacebookFeedForm;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FeedForm;
import pl.edu.agh.tai.project.feeds.service.FacebookService;

/**
 * Created by Michal on 2015-07-04.
 */
@Component
public class FacebookFeedValidator implements Validator {
    @Autowired
    private FacebookService facebookService;

    @Override
    public boolean supports(Class<?> aClass) {
        return FacebookFeedForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final FacebookFeedForm feedForm = (FacebookFeedForm) object;
        validatePrivacy(feedForm, errors);
    }

    private void validatePrivacy(FacebookFeedForm feedForm, Errors errors) {
        if (facebookService.getEvent(feedForm.getEventId()).getPrivacy() != Event.Privacy.OPEN) {
            errors.rejectValue("eventId", "form.facebookFeed.privacy.notOpen");
        }
    }
}
