package pl.edu.agh.tai.project.feeds.service;

import org.springframework.social.facebook.api.Event;
import org.springframework.social.facebook.api.Post;

import java.util.List;

/**
 * Created by Michal on 2015-06-30.
 */
public interface FacebookService {
    void getFeeds();

    void create();

    List<Event> searchEvent(String query);

    Event getEvent(String eventId);

    List<Post> getPostForEvent(String eventId);
}
