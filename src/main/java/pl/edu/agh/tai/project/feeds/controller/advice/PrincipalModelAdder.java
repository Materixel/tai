package pl.edu.agh.tai.project.feeds.controller.advice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.UserService;

import java.security.Principal;


/**
 * Created by Michal on 2015-07-05.
 */
@ControllerAdvice
public class PrincipalModelAdder {
    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public User getCurrentEdition (Principal principal) {
        if (principal != null) {
            try {
                return userService.findByUsername(principal.getName());
            } catch (EntityNotExistsException e) {
                return null;
            }
        } else {
            return null;
        }
    }
}
