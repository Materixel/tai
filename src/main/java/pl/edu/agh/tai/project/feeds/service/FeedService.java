package pl.edu.agh.tai.project.feeds.service;

import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;

import java.util.List;

/**
 * Created by Michal on 2015-07-01.
 */
public interface FeedService {
    List<Feed> getFeeds();

    Feed get(Integer feedId) throws EntityNotExistsException;
}
