package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CreateAccountForm;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.exception.UserAlreadyExistsException;
import pl.edu.agh.tai.project.feeds.model.Role;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.UserService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public User findByUsername(String username) throws EntityNotExistsException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotExistsException();
        }
        return user;
    }

    @Override
    @Transactional
    public List<User> list() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void save(User user) throws UserAlreadyExistsException {
        try {
            userRepository.save(user);
        } catch (JpaSystemException e) {
            throw new UserAlreadyExistsException();
        }
    }

    @Override
    @Transactional
    public User get(Integer id) throws EntityNotExistsException {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotExistsException("User with id " + id + " doesn't exist");
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
