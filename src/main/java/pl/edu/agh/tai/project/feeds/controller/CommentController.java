package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CommentForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Comment;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.CommentService;
import pl.edu.agh.tai.project.feeds.service.PostService;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Janusz on 2015-06-05.
 */
@Controller
@RequestMapping("feeds/{feed_id}/posts/{postId}/comments")
public class CommentController {

    @Autowired
    CommentService commentService;

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @ModelAttribute("commentForm")
    public CommentForm addCommentFormToModel(@PathVariable Map<String, String> params) {
        CommentForm form = new CommentForm();
        if (params.containsKey("comment_id")) {
            final Comment comment;
            try {
                comment = commentService.get(Integer.valueOf(params.get("comment_id")));
            } catch (EntityNotExistsException | NumberFormatException e) {
                return form;
            }
            form.setContent(comment.getContent());
        }
        return form;
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping
    public String listComments(Model model, @PathVariable Integer postId, RedirectAttributes redirectAttributes) {
        List<Comment> comments = commentService.findByPost_Id(postId);
        Post post = null;
        try {
            post = postService.get(postId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        model.addAttribute("comments", comments);
        model.addAttribute("feed", post.getFeed());
        model.addAttribute("post", post);
        return "comments/index";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreateCommentForm() {
        return "comments/create";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{comment_id}/edit", method = RequestMethod.GET)
    public String showEditCommentForm(RedirectAttributes redirectAttributes, @PathVariable("comment_id") Integer commentId) {
        try {
            commentService.get(commentId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Komentarz o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        return "comments/edit";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createComment(@Valid @ModelAttribute("commentForm") CommentForm commentForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes,
                             Principal principal, @PathVariable("postId") Integer postId) {

        if (bindingResult.hasErrors()) {
            return handleFormRedirect(commentForm, "create", bindingResult, redirectAttributes);
        }


        Comment newComment = new Comment();
        newComment.setContent(commentForm.getContent());
        newComment.setCreationDate(Calendar.getInstance().getTime());

        String username = principal.getName(); //get logged in username
        User creator = null;
        try {
            creator = userService.findByUsername(username);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }
        newComment.setCreator(creator);

        Post parentPost;
        try {
            parentPost = postService.get(postId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        newComment.setPost(parentPost);
        parentPost.addComment(newComment);
        commentService.save(newComment);

        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{comment_id}/edit", method = RequestMethod.POST)
    public String editPost(@Valid @ModelAttribute("commentForm") CommentForm commentForm,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes, @PathVariable("comment_id") Integer commentId){

        if (bindingResult.hasErrors()) {
            return handleFormRedirect(commentForm, "edit", bindingResult, redirectAttributes);
        }

        Comment editedComment;
        try {
            editedComment = commentService.get(commentId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Komentarz o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }

        //assume that we don't change creator, as finally only creator should be able to edit.
        editedComment.setContent(commentForm.getContent());
        editedComment.setLastEditionDate(new Date());

        commentService.save(editedComment);

        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{comment_id}/delete", method = RequestMethod.GET)
    public String deleteComment(@PathVariable("comment_id") Integer commentId, RedirectAttributes redirectAttributes) {
        try {
            commentService.delete(commentService.get(commentId));
        } catch (DataIntegrityViolationException e) {
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/feeds/{feed_id}";
        } catch (EntityNotExistsException e1) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Komentarz o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        redirectAttributes.addFlashAttribute("successMessage", "Komentarz zosta� usuni�ty");
        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{comment_id}", method = RequestMethod.GET)
    public String showPost(@PathVariable("comment_id") Integer commentId, Model model,
                           RedirectAttributes redirectAttributes) {
        Comment comment;
        try {
            comment = commentService.get(commentId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Komentarz o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        model.addAttribute("comment", comment);
        return "comments/show";
    }

    private String handleFormRedirect(CommentForm commentForm, String location, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", true);
        redirectAttributes.addFlashAttribute("commentForm", commentForm);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.commentForm",
                bindingResult);
        return "redirect:" + location;
    }
}
