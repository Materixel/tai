package pl.edu.agh.tai.project.feeds.service;

import org.springframework.security.access.prepost.PreAuthorize;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */
public interface PostService {

    List<Post> list();

    List<Post> findByFeed_Id(Integer id);

    void save(Post post);

    Post get(Integer id) throws EntityNotExistsException;

    void delete(Post post) throws EntityNotExistsException;
}
