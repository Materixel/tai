package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.CommentRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Comment;
import pl.edu.agh.tai.project.feeds.service.CommentService;

import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */
@Service
public class CommentServiceImpl implements CommentService {


    @Autowired
    private CommentRepository commentRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Comment> list() {
        return commentRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Comment> findByPost_Id(Integer id) {
        return commentRepository.findByPost_Id(id);
    }

    @Override
    @Transactional
    @PreAuthorize("#comment.creator.username == principal.username")
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    @Transactional
    public Comment get(Integer id) throws EntityNotExistsException {

        Comment comment = commentRepository.findOne(id);
        if (comment == null) {
            throw new EntityNotExistsException("Comment with id " + id + " doesn't exist");
        }
        return comment;
    }

    @Override
    @Transactional
    @PreAuthorize("#comment.creator.username == principal.username")
    public void delete(Comment comment) throws EntityNotExistsException {
        //To check if it exists.
        commentRepository.delete(comment);
    }


}
