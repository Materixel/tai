package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.PostForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.FeedService;
import pl.edu.agh.tai.project.feeds.service.LocalFeedService;
import pl.edu.agh.tai.project.feeds.service.PostService;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("feeds/{feed_id}/posts")
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    FeedService feedService;

    @Autowired
    UserService userService;

    @Autowired
    @Qualifier("postFormValidator")
    private Validator validator;

    @InitBinder("postForm")
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(validator);
    }

    @ModelAttribute("postForm")
    public PostForm addPostFormToModel(@PathVariable Map<String, String> params) {
        PostForm form = new PostForm();
       if (params.containsKey("post_id")) {
            final Post post;
            try {
                post = postService.get(Integer.valueOf(params.get("post_id")));
            } catch (EntityNotExistsException | NumberFormatException e) {
                return form;
            }
            form.setContent(post.getContent());
        }
        return form;
    }

    @RequestMapping
    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    public String listPosts(Model model, @PathVariable("feed_id") Integer feedId) {
        Feed feed = null;
        try {
            feed = feedService.get(feedId);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }
        List<Post> posts = postService.findByFeed_Id(feedId);
        model.addAttribute("feed", feed);
        model.addAttribute("posts", posts);
        return "posts/index";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreatePostForm() {
        return "posts/create";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{post_id}/edit", method = RequestMethod.GET)
    public String showEditPostForm(@PathVariable("post_id") Integer postId, RedirectAttributes redirectAttributes) {
        try {
            postService.get(postId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        return "posts/edit";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createPost(@Valid @ModelAttribute("postForm") PostForm postForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes,
                             Principal principal, @PathVariable("feed_id") Integer feedId) {
        if (bindingResult.hasErrors()) {
            return handleFormRedirect(postForm, "create", bindingResult, redirectAttributes);
        }

        Post newPost = new Post();
        newPost.setCreationDate(new Date());
        newPost.setContent(postForm.getContent());

        String username = principal.getName(); //get logged in username
        User creator = null;
        try {
            creator = userService.findByUsername(username);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }
        newPost.setCreator(creator);

        Feed parentFeed;
        try {
            parentFeed = feedService.get(feedId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Feed o podanym id nie istnieje");
            return "redirect:/feeds";
        }
        parentFeed.addPost(newPost);
        newPost.setFeed(parentFeed);
        postService.save(newPost);
        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{post_id}/edit", method = RequestMethod.POST)
    public String editPost(@Valid @ModelAttribute("postForm") PostForm postForm,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes,
                             Principal principal, @PathVariable("feed_id") Integer feedId,
                             @PathVariable("post_id") Integer postId){

        if (bindingResult.hasErrors()) {
            return handleFormRedirect(postForm, "edit", bindingResult, redirectAttributes);
        }

        Post editedPost;
        try {
            editedPost = postService.get(postId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }

        //assume that we don't change creator, as finally only creator should be able to edit.
        editedPost.setContent(postForm.getContent());
        editedPost.setLastEditionDate(new Date());

        postService.save(editedPost);
        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{post_id}/delete", method = RequestMethod.GET)
    public String deletePost(@PathVariable("post_id") Integer postId, RedirectAttributes redirectAttributes) {
        try {
            postService.delete(postService.get(postId));
        } catch (DataIntegrityViolationException e) {
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/feeds/{feed_id}";
        } catch (EntityNotExistsException e1) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        redirectAttributes.addFlashAttribute("success", true);
        redirectAttributes.addFlashAttribute("successMessage", "Post zosta� usuni�ty");
        return "redirect:/feeds/{feed_id}";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR"})
    @RequestMapping(value = "/{post_id}", method = RequestMethod.GET)
    public String showPost(@PathVariable("feed_id") Integer feedId, @PathVariable("post_id") Integer postId, Model model,
                           RedirectAttributes redirectAttributes) {
        Post post;
        Feed feed;
        try {
            post = postService.get(postId);
            feed = feedService.get(feedId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Post o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        model.addAttribute("post", post);
        model.addAttribute("feed", feed);
        return "posts/show";
    }

    private String handleFormRedirect(PostForm postForm, String location, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", true);
        redirectAttributes.addFlashAttribute("postForm", postForm);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.postForm",
                bindingResult);
        return "redirect:" + location;
    }
}
