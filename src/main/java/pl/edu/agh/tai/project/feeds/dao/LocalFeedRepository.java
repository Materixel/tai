package pl.edu.agh.tai.project.feeds.dao;

import org.springframework.cglib.core.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.List;

/**
 * Created by Michal on 2015-05-27.
 */
@Repository
public interface LocalFeedRepository extends JpaRepository<LocalFeed, Integer> {
    List<LocalFeed> findByCreator(User creator);
}
