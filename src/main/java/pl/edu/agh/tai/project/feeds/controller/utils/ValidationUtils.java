package pl.edu.agh.tai.project.feeds.controller.utils;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Janusz on 2015-06-18.
 */
@Component
public class ValidationUtils {
    private static final String USERNAME_REGEX = "^[a-zA-Z0-9-_.]+$";
    private static final Pattern USERNAME_PATTERN = Pattern.compile(USERNAME_REGEX);
    private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);

    public static boolean isStringNotNullAndNonEmpty(String s) {
        return s != null && s.trim().length() > 0;
    }

    public static boolean isValidUsername(String username) {
        return USERNAME_PATTERN.matcher(username).matches();
    }

    public static boolean isValidEmailAddress(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean isEndDateNotEarlierThanStart(Date startDate, Date endDate){
        if(startDate!=null && endDate!=null && startDate.after(endDate)){
            return false;
        }
        return true;
    }
}
