package pl.edu.agh.tai.project.feeds.controller.requestobject;

import pl.edu.agh.tai.project.feeds.model.Role;

import java.util.List;

/**
 * Created by Michal on 2015-06-08.
 */
public class CreateAccountForm {
    private String username;
    private String password;
    private String password2;
    private String email;
    private List<Role> roles;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}
