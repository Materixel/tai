package pl.edu.agh.tai.project.feeds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.tai.project.feeds.model.Comment;

import java.util.List;

/**
 * Created by Michal on 2015-05-27.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    List<Comment> findByPost_Id(Integer id);
}
