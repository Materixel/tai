package pl.edu.agh.tai.project.feeds.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.tai.project.feeds.model.Feed;

/**
 * Created by Michal on 2015-07-01.
 */
@Repository
public interface FeedRepository extends JpaRepository<Feed, Integer> {
}
