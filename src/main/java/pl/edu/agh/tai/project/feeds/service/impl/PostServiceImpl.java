package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.PostRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.PostService;

import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */
@Service
public class PostServiceImpl implements PostService {
    @Autowired
    PostRepository postRepository;

    @Override
    @Transactional
    public List<Post> list() {
        return postRepository.findAll();
    }

    @Override
    @Transactional
    public List<Post> findByFeed_Id(Integer id) {
        return postRepository.findByFeed_Id(id);
    }

    @Override
    @Transactional
    @PreAuthorize("#post.creator.username == principal.username")
    public void save(Post post) {
        postRepository.save(post);
    }

    @Override
    @Transactional
    public Post get(Integer id) throws EntityNotExistsException {
        Post post = postRepository.findOne(id);
        if (post == null) {
            throw new EntityNotExistsException("Post with id " + id + " doesn't exist");
        }
        return post;
    }

    @Override
    @PreAuthorize("#post.creator.username == principal.username")
    @Transactional
    public void delete(Post post) throws EntityNotExistsException {
        //To check if it exists.
        postRepository.delete(post);
    }
}
