package pl.edu.agh.tai.project.feeds.controller.requestobject;

/**
 * Created by Janusz on 2015-06-05.
 */
public class PostForm {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
