package pl.edu.agh.tai.project.feeds.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.edu.agh.tai.project.feeds.controller.requestobject.PostForm;
import pl.edu.agh.tai.project.feeds.controller.utils.ValidationUtils;

/**
 * Created by Janusz on 2015-06-28.
 */
@Component
public class PostFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PostForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final PostForm postForm = (PostForm) object;
        validateContent(postForm, errors);
    }

    private void validateContent(PostForm postForm, Errors errors) {
        if (!ValidationUtils.isStringNotNullAndNonEmpty(postForm.getContent())) {
            errors.rejectValue("content", "form.post.content.empty");
            return;
        }
    }
}
