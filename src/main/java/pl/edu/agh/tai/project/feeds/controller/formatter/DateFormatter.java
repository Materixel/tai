package pl.edu.agh.tai.project.feeds.controller.formatter;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Michal on 2015-07-01.
 */
@Component
public class DateFormatter implements Formatter<Date> {

    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        return DateFormat.getDateTimeInstance().parse(text);
    }

    @Override
    public String print(Date object, Locale locale) {
        return DateFormat.getDateTimeInstance().format(object);
    }
}
