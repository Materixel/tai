package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.social.facebook.api.*;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import pl.edu.agh.tai.project.feeds.service.FacebookService;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Michal on 2015-06-30.
 */
@Service
@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
public class FacebookServiceImpl implements FacebookService {

    @Autowired
    private Facebook facebook;

    @Override
    public void getFeeds() {
        User user = facebook.userOperations().getUserProfile();
        PagedList<Invitation> events = facebook.eventOperations().getAttending();
        events.size();
        user.getAbout();
        PagedList<Post> feeds = facebook.feedOperations().getFeed();
        System.out.println(feeds);
    }

    @Override
    public void create() {
        PagedList<Event> ss = facebook.eventOperations().search("test");
        ss.size();
    }

    @Override
    public List<Event> searchEvent(String query) {
        return facebook.eventOperations().search(query);
    }

    @Override
    public Event getEvent(String eventId) {
        return facebook.eventOperations().getEvent(eventId);
    }

    @Override
    public List<Post> getPostForEvent(String eventId) {
        return facebook.feedOperations().getFeed(eventId);
    }
}
