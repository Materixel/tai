package pl.edu.agh.tai.project.feeds.exception;

/**
 * Created by Michal on 2015-06-18.
 */
public class FieldErrorException extends Exception {
    private final String field;

    private final String errorKey;

    public FieldErrorException(String field, String errorKey) {
        super();
        this.field = field;
        this.errorKey = errorKey;
    }

    public String getField() {
        return field;
    }

    public String getErrorKey() {
        return errorKey;
    }
}
