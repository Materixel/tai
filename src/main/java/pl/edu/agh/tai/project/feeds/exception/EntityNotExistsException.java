package pl.edu.agh.tai.project.feeds.exception;

/**
 * Created by Janusz on 2015-06-05.
 */
public class EntityNotExistsException extends Exception {

    public EntityNotExistsException() {
        super();
    }

    public EntityNotExistsException(String message) {
        super(message);
    }
}
