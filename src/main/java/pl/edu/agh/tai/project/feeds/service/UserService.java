package pl.edu.agh.tai.project.feeds.service;

import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Service;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CreateAccountForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.exception.FieldErrorException;
import pl.edu.agh.tai.project.feeds.exception.UserAlreadyExistsException;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.List;

/**
 * Created by Janusz on 2015-06-05.
 */
public interface UserService {

    User findByUsername(String username) throws EntityNotExistsException;

    List<User> list();

    void save(User user) throws UserAlreadyExistsException;

    User get(Integer id) throws EntityNotExistsException;

    User findByEmail(String email);
}
