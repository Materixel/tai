package pl.edu.agh.tai.project.feeds.controller.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CommentForm;
import pl.edu.agh.tai.project.feeds.controller.utils.ValidationUtils;

/**
 * Created by Janusz on 2015-06-28.
 */
public class CommentFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return CommentForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final CommentForm commentForm = (CommentForm) object;
        validateContent(commentForm, errors);
    }

    private void validateContent(CommentForm commentForm, Errors errors) {
        if (!ValidationUtils.isStringNotNullAndNonEmpty(commentForm.getContent())) {
            errors.rejectValue("content", "form.comment.content.empty");
            return;
        }
    }
}
