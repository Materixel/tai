package pl.edu.agh.tai.project.feeds.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * Created by Michal on 2015-05-27.
 */
public enum Role implements GrantedAuthority {
    COMMENTER("Komentator"), CREATOR("Tw�rca"), FACEBOOK("Facebook");

    private final String description;


    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + this.name();
    }


}
