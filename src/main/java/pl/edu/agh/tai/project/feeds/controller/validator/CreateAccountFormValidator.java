package pl.edu.agh.tai.project.feeds.controller.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.edu.agh.tai.project.feeds.controller.requestobject.CreateAccountForm;
import pl.edu.agh.tai.project.feeds.controller.utils.ValidationUtils;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.service.UserService;

import java.util.List;
/**
 * Created by Janusz on 2015-06-11.
 */
@Component
public class CreateAccountFormValidator implements Validator {
    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return CreateAccountForm.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        final CreateAccountForm createAccountForm = (CreateAccountForm) object;
        validateUsername(createAccountForm, errors);
        validateEmail(createAccountForm, errors);
        validatePassword(createAccountForm, errors);
    }

    private void validateUsername(CreateAccountForm createAccountForm, Errors errors) {
        if (!ValidationUtils.isStringNotNullAndNonEmpty(createAccountForm.getUsername())) {
            errors.rejectValue("username", "form.account.username.empty");
            return;
        }

        if (!ValidationUtils.isValidUsername(createAccountForm.getUsername())) {
            errors.rejectValue("username", "form.account.username.invalid");
            return;
        }

        try {
            if(userService.findByUsername(createAccountForm.getUsername()) != null) {
                errors.rejectValue("username", "form.account.username.exists");
            }
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }

    }

    private void validateEmail(CreateAccountForm createAccountForm, Errors errors){
        if (!ValidationUtils.isStringNotNullAndNonEmpty(createAccountForm.getEmail())) {
            errors.rejectValue("email", "form.account.email.empty");
            return;
        }
        if (!ValidationUtils.isValidEmailAddress(createAccountForm.getEmail())) {
            errors.rejectValue("email", "form.account.email.invalid");
            return;
        }
        if (userService.findByEmail(createAccountForm.getEmail()) != null) {
            errors.rejectValue("email", "form.account.email.exists");
            return;
        }
    }

    private void validatePassword(CreateAccountForm createAccountForm, Errors errors) {
        if (createAccountForm.getPassword() == null) {
            errors.rejectValue("password", "form.account.password.empty");
            return;
        }
        if (createAccountForm.getPassword2() == null) {
            errors.rejectValue("password2", "form.account.password.empty");
            return;
        }
        if (!createAccountForm.getPassword2().equals(createAccountForm.getPassword())) {
            errors.rejectValue("password2", "form.account.password2.invalid");
        }
    }
}
