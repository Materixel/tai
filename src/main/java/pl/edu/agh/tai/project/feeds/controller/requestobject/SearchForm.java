package pl.edu.agh.tai.project.feeds.controller.requestobject;

/**
 * Created by Michal on 2015-07-01.
 */
public class SearchForm {
    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
