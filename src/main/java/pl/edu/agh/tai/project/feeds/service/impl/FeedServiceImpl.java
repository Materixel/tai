package pl.edu.agh.tai.project.feeds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.tai.project.feeds.dao.FeedRepository;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.service.FeedService;

import java.util.List;

/**
 * Created by Michal on 2015-07-01.
 */
@Service
public class FeedServiceImpl implements FeedService {

    @Autowired
    private FeedRepository feedRepository;

    @Override
    public List<Feed> getFeeds() {
        return feedRepository.findAll();
    }

    @Override
    public Feed get(Integer feedId) throws EntityNotExistsException {
        Feed feed = feedRepository.findOne(feedId);
        if (feed == null) {
            throw new EntityNotExistsException("Post with id " + feedId + " doesn't exist");
        }
        return feed;
    }
}
