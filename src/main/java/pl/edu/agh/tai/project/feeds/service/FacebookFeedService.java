package pl.edu.agh.tai.project.feeds.service;

import javafx.util.Pair;
import org.springframework.social.facebook.api.Event;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.FacebookFeed;

import java.util.List;

/**
 * Created by Michal on 2015-07-01.
 */
public interface FacebookFeedService {
    void save(FacebookFeed facebookFeed);

    List<Pair<Integer, Event>> listAsPairs();

    List<FacebookFeed> list();

    FacebookFeed getByEventId(String eventId);

    FacebookFeed get(Integer feedId) throws EntityNotExistsException;

    void delete(FacebookFeed facebookFeed);
}