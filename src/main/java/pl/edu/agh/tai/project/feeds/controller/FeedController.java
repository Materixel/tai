package pl.edu.agh.tai.project.feeds.controller;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.social.facebook.api.Event;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FeedForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.*;
import pl.edu.agh.tai.project.feeds.service.FacebookFeedService;
import pl.edu.agh.tai.project.feeds.service.FeedService;
import pl.edu.agh.tai.project.feeds.service.LocalFeedService;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * Created by Michal on 2015-06-04.
 */
@Controller
@RequestMapping("feeds")
public class FeedController {

    @Autowired
    private LocalFeedService localFeedService;

    @Autowired
    private FacebookFeedService facebookFeedService;

    @Autowired
    private FeedService feedService;

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR", "ROLE_FACEBOOK"})
    @RequestMapping
    public String listFeeds(Model model, Authentication authentication) {
        List<LocalFeed> localFeeds = localFeedService.list();
        model.addAttribute("localFeeds", localFeeds);
        if (authentication.getAuthorities().contains(Role.FACEBOOK)) {
            List<Pair<Integer, Event>> facebookEvents = facebookFeedService.listAsPairs();
            model.addAttribute("facebookFeeds", facebookEvents);
        }
        return "feeds/index";
    }

    @Secured({"ROLE_COMMENTER", "ROLE_CREATOR", "ROLE_FACEBOOK"})
    @RequestMapping("/{feed_id}")
    public String showFeed(@PathVariable("feed_id") Integer feedId) {
        try {
            facebookFeedService.get(feedId);
            return "redirect:facebook/{feed_id}";
        } catch (EntityNotExistsException e) {
            return "redirect:local/{feed_id}";
        }
    }
}
