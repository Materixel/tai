package pl.edu.agh.tai.project.feeds.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.social.facebook.api.Event;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.edu.agh.tai.project.feeds.controller.requestobject.FacebookFeedForm;
import pl.edu.agh.tai.project.feeds.controller.requestobject.SearchForm;
import pl.edu.agh.tai.project.feeds.exception.EntityNotExistsException;
import pl.edu.agh.tai.project.feeds.model.FacebookFeed;
import pl.edu.agh.tai.project.feeds.model.User;
import pl.edu.agh.tai.project.feeds.service.FacebookFeedService;
import pl.edu.agh.tai.project.feeds.service.FacebookService;
import pl.edu.agh.tai.project.feeds.service.UserService;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by Michal on 2015-07-01.
 */
@Controller
@RequestMapping("feeds/facebook")
public class FacebookFeedsController {

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private FacebookFeedService facebookFeedService;

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("facebookFeedValidator")
    private Validator validator;

    @InitBinder("facebookFeedForm")
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(validator);
    }

    @ModelAttribute("facebookFeedForm")
    public FacebookFeedForm addFacebookFeedFormToModel() {
        return new FacebookFeedForm();
    }

    @ModelAttribute("searchForm")
    public SearchForm addSearchFormToModel() {
        return new SearchForm();
    }


    @Secured({"ROLE_FACEBOOK"})
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreateFacebookFeedForm(@RequestParam(value = "query", required = false) String query, Model model) {
        if (query != null) {
            model.addAttribute("query", query);
            model.addAttribute("events", facebookService.searchEvent(query));
        }
        return "feeds/facebook/create";
    }

    @Secured({"ROLE_FACEBOOK"})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createFacebookFeed(@Valid @ModelAttribute("facebookFeedForm") FacebookFeedForm facebookFeedForm,
                                     BindingResult bindingResult, RedirectAttributes redirectAttributes, Principal principal) {
        if (bindingResult.hasErrors()) {
            return handleFormRedirect(facebookFeedForm, "create", bindingResult, redirectAttributes);
        }
        FacebookFeed facebookFeed = new FacebookFeed();
        String username = principal.getName(); //get logged in username
        User creator = null;
        try {
            creator = userService.findByUsername(username);
        } catch (EntityNotExistsException e) {
            e.printStackTrace();
        }
        facebookFeed.setCreator(creator);
        facebookFeed.setEventId(facebookFeedForm.getEventId());
        facebookFeedService.save(facebookFeed);
        return "redirect:/feeds";
    }

    @Secured("ROLE_FACEBOOK")
    @RequestMapping(value = "/{feed_id}")
    public String showFacebookFeed(@PathVariable("feed_id") Integer feedId, Model model, RedirectAttributes redirectAttributes) {
        FacebookFeed facebookFeed = null;
        try {
            facebookFeed = facebookFeedService.get(feedId);
        } catch (EntityNotExistsException e) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Feed o podanym id nie istnieje");
            return "redirect:/feeds/{feed_id}";
        }
        model.addAttribute("event", facebookService.getEvent(facebookFeed.getEventId()));
        model.addAttribute("postsFromFacebook", facebookService.getPostForEvent(facebookFeed.getEventId()));
        model.addAttribute("feed", facebookFeed);
        return "feeds/facebook/show";
    }

    @Secured({"ROLE_CREATOR"})
    @RequestMapping(value = "/{feed_id}/delete", method = RequestMethod.GET)
    public String deleteFeed(@PathVariable("feed_id") Integer feedId, RedirectAttributes redirectAttributes) {
        try {
            facebookFeedService.delete(facebookFeedService.get(feedId));
        } catch (DataIntegrityViolationException e) {
            redirectAttributes.addFlashAttribute("error", true);
            return "redirect:/feeds";
        } catch (EntityNotExistsException e1) {
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Feed o podanym id nie istnieje");
            return "redirect:/feeds";
        }
        redirectAttributes.addFlashAttribute("successMessage", "Feed zosta� usuni�ty");
        return "redirect:/feeds";
    }

    private String handleFormRedirect(FacebookFeedForm feedForm, String location, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", true);
        redirectAttributes.addFlashAttribute("facebookFeedForm", feedForm);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.facebookFeedForm",
                bindingResult);
        return "redirect:" + location;
    }
}
