<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ tag description="Page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Michał Suder, Janusz Węgrzyn">

    <title>Feeds</title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-theme.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="<c:url value="/resources/images/favicon.png"/>"/>

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/"/>">Feeds</a>
        </div>
        <div class="collapse navbar-collapse pull-right">
            <ul class="nav navbar-nav">
                <sec:authorize access="!isAuthenticated()">
                    <li>
                        <a href="<c:url value="/auth/login"/>">Zaloguj</a>
                    </li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <li>
                        <div class="navbar-brand">
                            Zalogowany jako ${user.name}
                        </div>
                    </li>
                    <li>
                        <form action="<c:url value="/auth/logout"/>" method="post">
                            <button type="submit" class="btn navbar-btn">Wyloguj</button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-left" style="margin-top: 50px;">
        <div class="panel panel-default">
            <div class="panel-body">
                <jsp:doBody/>
            </div>
        </div>
    </div>
</div>
<%--Scripts--%>
<script src="<c:url value="/resources/js/jquery-2.1.4.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
</body>
</html>