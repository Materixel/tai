<%@ tag description="Messages printer" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:if test="${error}">
    <div class="alert alert-danger">
        <i class="fa fa-warning"></i>
        <c:choose>
            <c:when test="${errorMessage != null}">
                ${errorMessage}
            </c:when>
            <c:otherwise>
                Wystąpił błąd
            </c:otherwise>
        </c:choose>
    </div>
</c:if>
<c:if test="${success}">
    <c:if test="${successMessage != null}">
        <div class="alert alert-success">
            <i class="fa fa-check"></i>
                ${successMessage}
        </div>
    </c:if>
</c:if>
<c:if test="${info}">
    <c:if test="${infoMessage != null}">
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i>
                ${infoMessage}
        </div>
    </c:if>
</c:if>