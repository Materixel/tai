<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:page>
  <jsp:body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.3&appId=686482594791128";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <h2 class="page-header">${event.name}</h2>
    <div class="col-lg-4 text-center">
          <c:forEach items="${postsFromFacebook}" var="post">
            <div class="fb-post" data-href="https://www.facebook.com/events/${event.id}/permalink/${post.id.split("_")[1]}" data-width="500"></div>
          </c:forEach>
    </div>
    <div class="col-lg-4">
      <t:alerts/>
      <ul class="list-group">
        <li class="list-group-item">Tytuł: ${event.name}</li>
        <li class="list-group-item">Opis: ${event.description}</li>
        <li class="list-group-item">Twórca: ${event.owner.name}</li>
        <li class="list-group-item">Lokacja: ${event.place.name}</li>
        <li class="list-group-item">Data rozpoczęcia: ${event.startTime.toLocaleString()}</li>
        <li class="list-group-item">Data zakończenia: ${event.endTime.toLocaleString()}</li>
      </ul>
      <sec:authorize access="#feed.creator.username == principal.username">
        <div>
          <a href="<c:url value="/feeds/facebook/${feed.id}/delete"/>"><button class="btn btn-block">Usuń</button></a>
        </div>
      </sec:authorize>
    </div>
    <div class="col-lg-4">
      <ul class="list-group">
        <c:forEach items="${feed.posts}" var="post">
          <li class="list-group-item">
            <div>
              ${post.creator.name}: ${post.content} <br/>
              Utworzenie: ${post.creationDate.toLocaleString()} <br/>
              <c:if test="${post.lastEditionDate != null}">
                Edycja: ${post.lastEditionDate.toLocaleString()}<br/>
              </c:if>
              <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/create" />">Odpowiedz</a>
              <sec:authorize access="#post.creator.username == principal.username">
                <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/edit" />">Edytuj</a>
                <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/delete" />">Usuń</a>
              </sec:authorize>
              <ul class="list-group">
                <c:forEach items="${post.comments}" var="comment">
                  <li class="list-group-item">
                    ${comment.creator.name}: ${comment.content}<br/>
                    Utworzenie: ${post.creationDate.toLocaleString()} <br/>
                    <c:if test="${post.lastEditionDate != null}">
                      Edycja: ${post.lastEditionDate.toLocaleString()}<br/>
                    </c:if>
                    <sec:authorize access="#comment.creator.username == principal.username">
                      <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/${comment.id}/edit" />">Edytuj</a>
                      <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/${comment.id}delete" />">Usuń</a>
                    </sec:authorize>
                  </li>
                </c:forEach>
              </ul>
            </div>
          </li>
        </c:forEach>
      </ul>
      <a class="btn btn-success" href="<c:url value="/feeds/${feed.id}/posts/create" />">Dodaj post</a>
    </div>
  </jsp:body>
</t:page>