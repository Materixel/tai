<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:page>
  <jsp:body>
    <h2 class="page-header">Tworzenie wydarzenia</h2>
    <t:alerts/>
    <div class="col-lg-12">
      <form:form action="create" method="GET" role="form" commandName="searchForm">
        <div class="form-group">
          <form:label path="query">Szukaj:</form:label>
          <form:input path="query" cssClass="form-control" value="${query}"/>
          <form:errors path="query" cssClass="label label-danger" element="span"/>
        </div>
        <button type="submit" class="btn btn-default">Szukaj</button>
      </form:form>
      <form:form action="create" method="POST" role="form" commandName="facebookFeedForm">
        <div class="form-group">
          <form:label path="eventId">Wydarzenia:</form:label>
          <form:select path="eventId" cssClass="form-control">
            <form:options items="${events}" itemValue="id" itemLabel="name"/>
          </form:select>
          <form:errors path="eventId" cssClass="label label-danger" element="span"/>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-default">Dodaj</button>
      </form:form>
    </div>
  </jsp:body>
</t:page>

