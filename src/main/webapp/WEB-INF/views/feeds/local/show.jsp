<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:page>
  <jsp:body>
    <h2 class="page-header">${feed.title}</h2>
    <t:alerts/>
    <div class="col-lg-6">
      <div style="margin-bottom: 10px">
        <ul class="list-group">
          <li class="list-group-item">Tytuł: ${feed.title}</li>
          <li class="list-group-item">Opis: ${feed.content}</li>
          <li class="list-group-item">Twórca: ${feed.creator.name}</li>
          <li class="list-group-item">Lokacja: ${feed.location}</li>
          <li class="list-group-item">Data rozpoczęcia: ${feed.startTime.toLocaleString()}</li>
          <li class="list-group-item">Data zakończenia: ${feed.endTime.toLocaleString()}</li>
        </ul>
      </div>
      <sec:authorize access="#feed.creator.username == principal.username">
        <div>
          <a href="<c:url value="/feeds/local/${feed.id}/edit"/>"><button class="btn btn-block">Edytuj</button></a>
          <a href="<c:url value="/feeds/local/${feed.id}/delete"/>"><button class="btn btn-block">Usuń</button></a>
        </div>
      </sec:authorize>
    </div>
    <div class="col-lg-6">
      <ul class="list-group">
        <c:forEach items="${feed.posts}" var="post">
          <li class="list-group-item">
            <div>
                ${post.creator.name}: ${post.content} <br/>
                Utworzenie: ${post.creationDate.toLocaleString()} <br/>
                <c:if test="${post.lastEditionDate != null}">
                  Edycja: ${post.lastEditionDate.toLocaleString()}<br/>
                </c:if>
                <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/create" />">Odpowiedz</a>
              <sec:authorize access="#post.creator.username == principal.username">
                <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/edit" />">Edytuj</a>
                <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/delete" />">Usuń</a>
              </sec:authorize>
              <ul class="list-group">
                <c:forEach items="${post.comments}" var="comment">
                  <li class="list-group-item">
                      ${comment.creator.name}: ${comment.content}<br/>
                      Utworzenie: ${comment.creationDate.toLocaleString()} <br/>
                      <c:if test="${comment.lastEditionDate != null}">
                        Edycja: ${comment.lastEditionDate.toLocaleString()}<br/>
                      </c:if>
                    <sec:authorize access="#comment.creator.username == principal.username">
                      <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/${comment.id}/edit" />">Edytuj</a>
                      <a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/${comment.id}delete" />">Usuń</a>
                    </sec:authorize>
                  </li>
                </c:forEach>
              </ul>
            </div>
          </li>
        </c:forEach>
      </ul>
      <a class="btn btn-success" href="<c:url value="/feeds/${feed.id}/posts/create" />">Dodaj post</a>
    </div>
  </jsp:body>
</t:page>