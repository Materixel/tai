<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:page>
  <jsp:body>
    <h2 class="page-header">Tworzenie feeda</h2>
    <t:alerts/>
    <div class="col-lg-12">
      <form:form action="create" method="POST" role="form" commandName="feedForm">
        <div class="form-group">
          <form:label path="title">Tytul:</form:label>
          <form:input path="title" cssClass="form-control"/>
          <form:errors path="title" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="content">Opis:</form:label>
          <form:textarea path="content" cssClass="form-control"/>
          <form:errors path="content" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="location">Lokalizacja:</form:label>
          <form:input path="location" cssClass="form-control"/>
          <form:errors path="location" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="startTime">Data rozpoczęcia:</form:label>
          <form:input path="startTime" cssClass="form-control"/>
          <form:errors path="startTime" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="endTime">Data zakończenia:</form:label>
          <form:input path="endTime" cssClass="form-control"/>
          <form:errors path="endTime" cssClass="label label-danger" element="span"/>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-default">Dodaj feeda</button>
        <button type="reset" class="btn btn-default">Wyczyść</button>
      </form:form>
    </div>
  </jsp:body>
</t:page>

