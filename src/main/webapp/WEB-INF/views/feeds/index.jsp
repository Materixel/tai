<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:page>
  <jsp:body>
    <t:alerts/>
    <div class="col-lg-6">

      <h2 class="page-header">Lokalne</h2>
      <ul class="list-group">
        <c:forEach items="${localFeeds}" var="localFeed">
          <li class="list-group-item"><a href="<c:url value="/feeds/local/${localFeed.id}"/>">${localFeed.title}</a></li>
        </c:forEach>
      </ul>
      <sec:authorize access="hasAuthority('ROLE_CREATOR') or hasAuthority('ROLE_FACEBOOK')">
        <div>
          <a class="btn btn-success" href="<c:url value="/feeds/local/create" />">Dodaj wydarzenie</a>
        </div>
      </sec:authorize>
    </div>
    <div class="col-lg-6">
      <sec:authorize access="hasAuthority('ROLE_FACEBOOK')" >
        <h2 class="page-header">Facebook</h2>
        <ul class="list-group">
          <c:forEach items="${facebookFeeds}" var="facebookFeed">
            <li class="list-group-item"><a href="<c:url value="/feeds/facebook/${facebookFeed.key}"/>">${facebookFeed.value.name}</a></li>
          </c:forEach>
        </ul>
        <a class="btn btn-success" href="<c:url value="/feeds/facebook/create" />">Dodaj wydarzenie z FB</a>
      </sec:authorize>
    </div>
  </jsp:body>
</t:page>