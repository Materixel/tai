<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
  <jsp:body>
    <div class="col-lg-4">
      <h2 class="page-header">Komentarz</h2>
      <t:alerts/>
      Opis: ${comment.content}
      Twórca: ${comment.creator}
      Data utworzenia: ${comment.creationDate.toLocaleString()}
      Ostatnia edycja: ${comment.lastEditionDate.toLocaleString()}
      <a role="button" class="btn btn-primary" href="<c:url value="${comment.id}/edit" />"><i
              class="fa fa-edit"></i> Edytuj
      </a>
      <a role="button" class="btn btn-danger" href="<c:url value="${comment.id}/delete" />"><i
              class="fa fa-trash"></i> Usuń
      </a>
    </div>
  </jsp:body>
</t:page>