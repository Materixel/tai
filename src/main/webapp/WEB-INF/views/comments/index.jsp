<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
  <jsp:body>
    <div class="col-lg-4">
      <h2 class="page-header">Komentarze</h2>
      <t:alerts/>
      <ul class="list-group">
        <c:forEach items="${comments}" var="comment">
          <li class="list-group-item"><a href="<c:url value="comments/${comment.id}"/>">${comment.content}</a></li>
        </c:forEach>
      </ul>
      <a class="btn btn-success" href="<c:url value="comments/create" />">Dodaj komentarz</a>
    </div>
  </jsp:body>
</t:page>