<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:page>
  <jsp:body>
    <sec:authorize access="isAnonymous()">
      <div class="col-lg-2">
      </div>
      <div class="col-lg-8 center-block" >
        <div style="margin-bottom: 10px;">
          <h2 class="page-header">Zaloguj</h2>
          <t:alerts/>
          <form:form method="POST" action="logincheck" role="form" commandName="createAccountForm">
            <div class="form-group">
              <form:label path="username">Nazwa użytkownika:</form:label>
              <form:input path="username" cssClass="form-control"/>
              <form:errors path="username" cssClass="label label-danger" element="span"/>
            </div>
            <div class="form-group">
              <form:label path="password">Hasło:</form:label>
              <form:password path="password" cssClass="form-control"/>
              <form:errors path="password" cssClass="label label-danger" element="span"/>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <button type="submit" class="btn btn-default">Zaloguj</button>
            <button type="reset" class="btn btn-default">Wyczyść</button>
            <a href="<c:url value="/auth/create"/>" class="btn btn-default" role="button">Utwórz konto</a>
          </form:form>
        </div>
        <div>
          <a href="<c:url value="/auth/facebook"/>"><button class="btn btn-block">Zaloguj się za pomocą Facebook</button></a>
        </div>
      </div>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
      <p>Redirect to main page</p>
    </sec:authorize>
  </jsp:body>
</t:page>