<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:page>
  <jsp:body>
    <h2 class="page-header">Tworzenie konta</h2>
    <t:alerts/>
    <div class="col-lg-2">
    </div>
    <div class="col-lg-8">
      <form:form action="create" method="POST" role="form" commandName="createAccountForm">
        <div class="form-group">
          <form:label path="username">Nazwa użytkownika:</form:label>
          <form:input path="username" cssClass="form-control"/>
          <form:errors path="username" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="password">Hasło:</form:label>
          <form:password path="password" cssClass="form-control"/>
          <form:errors path="password" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="password2">Powtórz hasło:</form:label>
          <form:password path="password2" cssClass="form-control"/>
          <form:errors path="password2" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="email">Email:</form:label>
          <form:input path="email" cssClass="form-control"/>
          <form:errors path="email" cssClass="label label-danger" element="span"/>
        </div>
        <div class="form-group">
          <form:label path="roles">Uprawnienia:</form:label>
          <form:select multiple="false" path="roles" cssClass="form-control">
            <c:forEach items="${roles}" var="item">
              <form:option value="${item}">${item.description}</form:option>
            </c:forEach>
          </form:select>
          <form:errors path="roles" cssClass="label label-danger"/>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-default">Utwórz konto</button>
        <button type="reset" class="btn btn-default">Wyczyść</button>
      </form:form>
    </div>
  </jsp:body>
</t:page>

