<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:page>
  <jsp:body>
    <h2 class="page-header">Tworzenie posta</h2>
    <t:alerts/>
    <div class="col-lg-12">
      <form:form action="create" method="POST" role="form" commandName="postForm">
        <div class="form-group">
          <form:label path="content">Zawartość:</form:label>
          <form:textarea path="content" cssClass="form-control"/>
          <form:errors path="content" cssClass="label label-danger" element="span"/>
        </div>
        <button type="submit" class="btn btn-default">Dodaj post</button>
        <button type="reset" class="btn btn-default">Wyczyść</button>
      </form:form>
    </div>
  </jsp:body>
</t:page>

