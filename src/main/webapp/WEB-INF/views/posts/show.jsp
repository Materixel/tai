<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
  <jsp:body>
    <div class="col-lg-4">
      <h2 class="page-header">Post</h2>
      <t:alerts/>
      Opis: ${post.content}
      Twórca: ${post.creator}
      Data utworzenia: ${post.creationDate.toLocaleString()}
      Ostatnia edycja: ${post.lastEditionDate.toLocaleString()}
      <ul class="list-group">
        <c:forEach items="${post.comments}" var="comment">
          <li class="list-group-item"><a href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/${comment.id}"/>">${comment.content}</a></li>
        </c:forEach>
      </ul>
      <a class="btn btn-success" href="<c:url value="/feeds/${feed.id}/posts/${post.id}/comments/create" />">Dodaj komentarz</a>
      <a role="button" class="btn btn-primary" href="<c:url value="/feeds/${feed.id}/posts/${post.id}/edit" />"><i
              class="fa fa-edit"></i> Edytuj
      </a>
      <a role="button" class="btn btn-danger" href="<c:url value="/feeds/${feed.id}/posts/${post.id}/delete" />"><i
              class="fa fa-trash"></i> Usuń
      </a>

    </div>
  </jsp:body>
</t:page>