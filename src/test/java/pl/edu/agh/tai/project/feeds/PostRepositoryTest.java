package pl.edu.agh.tai.project.feeds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.LocalFeedRepository;
import pl.edu.agh.tai.project.feeds.dao.PostRepository;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/test-context.xml"})
public class PostRepositoryTest {

    @Autowired
    private LocalFeedRepository localFeedRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    private static final String USERNAME = "USERNAME";

    private Integer localFeedId;

    private User user;
    @Before
    @Transactional
    public void before() {
        user = new User();
        user.setEmail("TEST@EMAIL.PL");
        user.setPassword("PASSWORD");
        user.setUsername(USERNAME);
        userRepository.save(user);

        LocalFeed localFeed = new LocalFeed();
        localFeed.setCreator(user);
        localFeed.setTitle("TITLE");
        localFeed.setContent("CONTENT");
        localFeedRepository.save(localFeed);
        localFeedId = localFeed.getId();
    }

    @Test
    @Transactional
    public void testSave() {
        LocalFeed localFeed= localFeedRepository.findOne(localFeedId);
        Post post = new Post();
        post.setCreationDate(new Date());
        post.setCreator(user);
        post.setContent("CONTENT");
        post.setFeed(localFeed);
        localFeed.getPosts().add(post);
        postRepository.save(post);

        Post readedPost = postRepository.findOne(post.getId());
        LocalFeed readedFeed = localFeedRepository.findOne(localFeedId);

        Assert.assertNotNull(readedPost);
        Assert.assertEquals(readedFeed.getPosts().size(), 1);
    }
}
