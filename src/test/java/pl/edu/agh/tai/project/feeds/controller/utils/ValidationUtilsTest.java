package pl.edu.agh.tai.project.feeds.controller.utils;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by Janusz on 2015-07-04.
 */
public class ValidationUtilsTest {
    @Test
    public void isStringNotNullAndNonEmptyShouldReturnFalseWhenGivenNull() throws Exception {
        // given
        final String s = null;

        // when
        final boolean result = ValidationUtils.isStringNotNullAndNonEmpty(s);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void isStringNotNullAndNonEmptyShouldReturnFalseWhenGivenEmptyString() throws Exception {
        // given
        final String s = "";

        // when
        final boolean result = ValidationUtils.isStringNotNullAndNonEmpty(s);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void isStringNotNullAndNonEmptyShouldReturnFalseWhenGivenOnlySpaces() throws Exception {
        // given
        final String s = "        ";

        // when
        final boolean result = ValidationUtils.isStringNotNullAndNonEmpty(s);

        // then
        assertThat(result).isFalse();
    }

    @Test
    public void isStringNotNullAndNonEmptyShouldReturnTrueWhenGivenNonEmptyString() throws Exception {
        // given
        final String s = "a";

        // when
        final boolean result = ValidationUtils.isStringNotNullAndNonEmpty(s);

        // then
        assertThat(result).isTrue();
    }

    @Test
    public void isEndDateNotEarlierThanStartShouldReturnTrueWhenGivenEqualDates(){
        //given
        Date startTime = null;
        Date endTime = null;
        try {
            startTime = DateFormat.getDateTimeInstance().parse("2015-06-13 16:00:00");
            endTime = DateFormat.getDateTimeInstance().parse("2015-06-13 16:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //when
        final boolean result = ValidationUtils.isEndDateNotEarlierThanStart(startTime, endTime);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void isEndDateNotEarlierThanStartShouldReturnTrueWhenGivenProperDates(){
        //given
        Date startTime = null;
        Date endTime = null;
        try {
            startTime = DateFormat.getDateTimeInstance().parse("2015-06-13 16:00:00");
            endTime = DateFormat.getDateTimeInstance().parse("2015-06-13 16:05:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //when
        final boolean result = ValidationUtils.isEndDateNotEarlierThanStart(startTime, endTime);

        //then
        assertThat(result).isTrue();
    }

    @Test
    public void isEndDateNotEarlierThanStartShouldReturnTrueWhenGivenNullDates(){
        //given
        Date startTime = null;
        Date endTime = null;

        //when
        final boolean result = ValidationUtils.isEndDateNotEarlierThanStart(startTime, endTime);

        //then
        assertThat(result).isTrue();
    }


    @Test
    public void isEndDateNotEarlierThanStartShouldReturnFalseWhenGivenWrongDates(){
        //given
        Date startTime = null;
        Date endTime = null;
        try {
            startTime = DateFormat.getDateTimeInstance().parse("2015-06-13 16:00:00");
            endTime = DateFormat.getDateTimeInstance().parse("2015-06-13 15:05:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //when
        final boolean result = ValidationUtils.isEndDateNotEarlierThanStart(startTime, endTime);

        //then
        assertThat(result).isFalse();
    }
}
