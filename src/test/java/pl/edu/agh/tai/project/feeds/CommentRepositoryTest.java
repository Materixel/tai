package pl.edu.agh.tai.project.feeds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.CommentRepository;
import pl.edu.agh.tai.project.feeds.dao.LocalFeedRepository;
import pl.edu.agh.tai.project.feeds.dao.PostRepository;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.model.Comment;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.Post;
import pl.edu.agh.tai.project.feeds.model.User;

import java.util.Date;

/**
 * Created by Janusz on 2015-07-06.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/test-context.xml"})
public class CommentRepositoryTest {

    @Autowired
    private LocalFeedRepository localFeedRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    private User user;

    private static final String USERNAME = "USERNAME";

    private Integer commentId;

    private Integer postId;


    @Before
    @Transactional
    public void before() {
        user = new User();
        user.setEmail("TEST@EMAIL.PL");
        user.setPassword("PASSWORD");
        user.setUsername(USERNAME);
        userRepository.save(user);

        LocalFeed localFeed = new LocalFeed();
        localFeed.setCreator(user);
        localFeed.setTitle("TITLE");
        localFeed.setContent("CONTENT");
        localFeedRepository.save(localFeed);

        Post post = new Post();
        post.setCreationDate(new Date());
        post.setCreator(user);
        post.setContent("CONTENT");
        post.setFeed(localFeed);
        localFeed.getPosts().add(post);
        postRepository.save(post);
        postId = post.getId();
    }

    @Test
    @Transactional
    public void testSave() {
        Post post= postRepository.findOne(postId);
        Comment comment = new Comment();
        comment.setCreationDate(new Date());
        comment.setCreator(user);
        comment.setContent("CONTENT");
        comment.setPost(post);
        post.getComments().add(comment);
        commentRepository.save(comment);
        commentId = comment.getId();

        Post readedPost = postRepository.findOne(postId);
        Comment readedComment = commentRepository.findOne(commentId);

        Assert.assertNotNull(readedComment);
        Assert.assertEquals(readedPost.getComments().size(), 1);
    }
}

