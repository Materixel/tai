package pl.edu.agh.tai.project.feeds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.LocalFeedRepository;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.model.Feed;
import pl.edu.agh.tai.project.feeds.model.LocalFeed;
import pl.edu.agh.tai.project.feeds.model.User;

/**
 * Created by Michal on 2015-06-05.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/test-context.xml"})
public class LocalFeedRepositoryTest {
    @Autowired
    private LocalFeedRepository localFeedRepository;

    @Autowired
    private UserRepository userRepository;

    private static final String USERNAME = "USERNAME";

    @Before
    @Transactional
    public void before() {
        User user = new User();
        user.setEmail("TEST@EMAIL.PL");
        user.setPassword("PASSWORD");
        user.setUsername(USERNAME);
        userRepository.save(user);
    }

    @Test
    @Transactional
    public void testSave() {
        LocalFeed feed = new LocalFeed();
        feed.setContent("CONTENT");
        feed.setTitle("TITLE");
        User user = userRepository.findByUsername(USERNAME);
        feed.setCreator(user);
        user.getFeeds().add(feed);
        localFeedRepository.save(feed);
        user = userRepository.findByUsername(USERNAME);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getFeeds());
        Assert.assertEquals(user.getFeeds().size(), 1);
    }
}
