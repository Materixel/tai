package pl.edu.agh.tai.project.feeds;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.tai.project.feeds.dao.UserRepository;
import pl.edu.agh.tai.project.feeds.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/test-context.xml"})
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    private static final String USERNAME = "USERNAME";

    private User user;

    @Before
    @Transactional
    public void before() {
        user = new User();
        user.setEmail("TEST@EMAIL.PL");
        user.setPassword("PASSWORD");
        user.setUsername(USERNAME);
        userRepository.save(user);
    }


    @Test
    @Transactional
    public void testUserIsExisting() {
        Assert.assertEquals(1, userRepository.findAll().size());
    }

    @Test
    @Transactional
    public void testFindUserByName() {
        User user = userRepository.findByUsername(USERNAME);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getPassword(), "PASSWORD");
        Assert.assertEquals(user.getEmail(), "TEST@EMAIL.PL");
    }

    @Test
    @Transactional
    public void testDeleteUser(){
        userRepository.delete(user);
        Assert.assertEquals(0, userRepository.findAll().size());
    }

}
